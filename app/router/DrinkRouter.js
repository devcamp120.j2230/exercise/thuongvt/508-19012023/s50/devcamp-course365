//Khai báo thư viện express
const express = require("express");
// Khai báo Middleware 
const {
    getAllDrinkMiddleware,
    getADrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} =  require (`../middleware/DrinkMiddleware`);

// Tạo router
const DrinkRouter = express.Router();

// Sử dụng router
DrinkRouter.get("/Drink",getAllDrinkMiddleware,(req,res)=>{
    res.json({
        message: "get all drink"
    })
});

DrinkRouter.get("/Drink/:DrinkId", getADrinkMiddleware, (req,res)=>{
    let DrinkId = req.params.DrinkId;
    res.json({
        message: `DrinkId = ${DrinkId}`
    })
})

DrinkRouter.post("/Drink",postDrinkMiddleware,(req,res)=>{
    res.json({
        message: "post a drink"
    })
})

DrinkRouter.put("/Drink/:DrinkId",putDrinkMiddleware,(req,res)=>{
    let DrinkId = req.params.DrinkId;
    res.json({
        message: `DrinkId = ${DrinkId}`
    })
})

DrinkRouter.delete("/Drink/:DrinkId",deleteDrinkMiddleware,(req,res)=>{
    let DrinkId = req.params.DrinkId;
    res.json({
        message: `DrinkId = ${DrinkId}`
    })
})

module.exports = {DrinkRouter}
