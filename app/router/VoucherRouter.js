//Khai báo thư viện express
const express = require("express");

// Khai báo Middleware 
const {
    getAVoucherMiddleware,
    getAllVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
} =  require (`../middleware/VoucherMiddleware`);

// Tạo router
const VoucherRouter = express.Router();

// Sử dụng router
VoucherRouter.get("/Voucher",getAllVoucherMiddleware,(req,res)=>{
    res.json({
        message:"Get all voucher"
    })
});

VoucherRouter.get("/Voucher/:VoucherId",getAVoucherMiddleware,(req,res)=>{
    let VoucherId = req.params.VoucherId;
    res.json({
        message:`VoucherId = ${VoucherId}`
    })
});

VoucherRouter.post("/Voucher",postVoucherMiddleware, (req,res)=>{
    res.json({
        message: "post a voucher"
    })
});

VoucherRouter.put("/Voucher/:VoucherId",putVoucherMiddleware,(req,res)=>{
    let VoucherId = req.params.VoucherId;
    res.json({
        message:`VoucherId = ${VoucherId}`
    })
});

VoucherRouter.delete("/Voucher/:VoucherId",deleteVoucherMiddleware,(req,res)=>{
    let VoucherId = req.params.VoucherId;
    res.json({
        message:`VoucherId = ${VoucherId}`
    })
})

module.exports = {VoucherRouter}
