// khai báo thư viện mongoose
const mogoose = require("mongoose");

// khai báo class schema của mongo
const schema = mogoose.Schema;

//khai báo  review schema 
const userSchema = new schema({
    // trường id
    _idUser: {
        type: mogoose.Types.ObjectId,
        unique: true
    },
    // trường fullName
    fullName:{
        type: String,
        required: true
    },
    // trường Email
    email:{
        type: String,
        required: true,
        unique:true
    },
    // trường address
    address:{
        type: String,
        required: true
    },
    // trường phone 
    phone:{
        type: String,
        required: true,
        unique:true
    },
    // trường orders
    orders:[
        {
            type: mogoose.Types.ObjectId,
            ref:"Order"
        }
    ],
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },
    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
})

module.exports = mogoose.model("User", userSchema);
