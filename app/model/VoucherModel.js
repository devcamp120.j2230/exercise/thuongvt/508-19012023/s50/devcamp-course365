// khai báo thư viện mongoose
const mogoose = require("mongoose");

// khai báo class schema của mongo
const schema = mogoose.Schema;

// khai báo  review schema 
const voucherSchema = new schema({
    // trường id
    _idVoucher: {
        type: mogoose.Types.ObjectId,
        unique: true
    },
    // trường mã giảm giá
    maVoucher:{
        type: String,
        unique: true,
        required: true
    },
    // trường phần trăm giảm giá
    phanTramGiamGia:{
        type: Number,
        required: true
    },
    // trường ghi chú
    ghiChu:{
        type: String,
        required: false
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },
    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
})

module.exports = mogoose.model("Voucher", voucherSchema);
