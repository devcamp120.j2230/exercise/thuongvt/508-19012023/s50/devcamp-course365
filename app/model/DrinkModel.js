// import thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của thư viện
const schema = mongoose.Schema; //giống như khai báo class trong ES6

// Khai báo drink schema
const drinkSchema = new schema ({
    //Trường Id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    //Trường mã nước uống
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    // trường đơn giá
    donGia:{
        type: Number,
        required: true
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },
    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
});
module.exports = mongoose.model("Drink",drinkSchema)