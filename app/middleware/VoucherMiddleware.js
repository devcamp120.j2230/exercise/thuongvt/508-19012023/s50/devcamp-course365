const getAllVoucherMiddleware = (req,res,next)=>{ 
    console.log("get all Voucher");
    next()
}

const getAVoucherMiddleware = (req,res,next)=>{
    console.log("get a Voucher");
    next();
}

const postVoucherMiddleware = (req,res,next)=>{
    console.log("post a Voucher");
    next();
}

const putVoucherMiddleware = (req,res,next)=>{
    console.log("put a Voucher");
    next();
}

const deleteVoucherMiddleware = (req,res,next)=>{
    console.log("delete a Voucher");
    next();
}

module.exports = {
    getAllVoucherMiddleware,
    getAVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
}
